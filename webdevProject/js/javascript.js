document.addEventListener("DOMContentLoaded", function(){

    //object for reg pattern
    let validation= [
        {
            name:"Alphanumeric",
            regpattern:/^[a-z0-9\s]+$/i,
            appliesTo:[
                "projid", "description", "title"
            ]
        },
        {
            name:"Alphabetic",
            regpattern:/^[A-Za-z\s]+$/,
            appliesTo:[
                "ownname", "category", "status"
            ]
        },
        {
            name:"Numeric",
            regpattern:/^\d+$/,
            appliesTo:[
                "hours", "rate"
            ]
        }
    ]

    //going through the input forms and adding an event listener
    let formInputs= [
        document.getElementById("projid"),
        document.getElementById("ownname"),
        document.getElementById("title"),
        document.getElementById("category"),
        document.getElementById("hours"),
        document.getElementById("rate"),
        document.getElementById("status"),
        document.getElementById("description"),
    ]
    for(let formInput of formInputs){
        formInput.addEventListener("change", function(e){
            validate(e, validation, formInputs)
        })
    }
    

    // Object constructor for entries
    function projectEntry(ID, Ownername, title, category, hours, rate, status, description){
        this.proj_id = ID
        this.owner = Ownername
        this.proj_title = title
        this.proj_category = category
        this.nmbr_hours = hours
        this.rate = rate
        this.proj_status = status
        this.description = description
    }

    //create an empty array list of projects to be filled later
    var projectList = []

    // reset button
    let resetButton = document.getElementById("resetButton")
    resetButton.addEventListener("click", function(){
        reset(formInputs)}, false)

    // add button
    let addButton = document.getElementById("addButton")
    addButton.addEventListener("click", function(e){

        // create new object using constructor and form values, then pushes it to the arraylist of projectd
        let entryToAdd = new projectEntry(formInputs[0].value, formInputs[1].value, formInputs[2].value, formInputs[3].value, formInputs[4].value, formInputs[5].value, formInputs[6].value, formInputs[7].value)
        projectList.push(entryToAdd);
        
        // add project to the table using buildTable function
        buildTable(projectList, projectEntry)
        
        // resets the form fields after adding object
        reset(formInputs)

        document.getElementById("status-bar").innerHTML = "Project added to table"
        })

        
        
    

    //other buttons

    //append
    let appendButton= document.getElementById("append")
    appendButton.onclick= function(){
        for(let project of projectList){
            let jsonFormat= JSON.stringify(project)
            localStorage.setItem(" project: "+project.proj_id, jsonFormat)
        }
        document.getElementById("status-bar").innerHTML = "Appended to local storage"
    }
    
    //clear
    let clearButton= document.getElementById("clear")
    clearButton.onclick= function(){
        localStorage.clear()
        document.getElementById("status-bar").innerHTML = "Local Storage cleared"
    }

    //write
    let writeButton= document.getElementById("write")
    writeButton.onclick= function(){
        localStorage.clear()
        for(let project of projectList){
            let jsonFormat= JSON.stringify(project)
            localStorage.setItem(" project: "+project.proj_id, jsonFormat)
        }
        let storageWritten = projectList.length
        document.getElementById("status-bar").innerHTML = "Saving " + storageWritten + " project(s) into the local storage"
    }

    //load
    let loadButton= document.getElementById("load")
    loadButton.onclick= function(){
        for (let i = 0; i < localStorage.length; i++){
            let jsonFormat= localStorage.getItem(localStorage.key(i))
            let project= JSON.parse(jsonFormat)

            //make sure to not load something already loaded
            let projectExists= projectList.filter(projectEntry => {
                return projectEntry.proj_id == project.proj_id
            })
            if(projectExists.length==0){
                projectList.push(project)    
            }    
        }
        //recreate table with the new projectList array
        buildTable(projectList, projectEntry)

        let storageLength = localStorage.length
        document.getElementById("status-bar").innerHTML = "Local Storage loaded " + storageLength + " item(s)"
    }





    //select query add event to check if pattern exists in any row
    let selectQuery= document.getElementById("query")
    
    selectQuery.addEventListener("keyup", function(e){
        let newProjectList= []
        let queryString= e.target.value
        let queryRegPattern= new RegExp(".*" + queryString + ".*")
        
        projectList.forEach(element => {
            let isThere= false
            for(let i in element){
                if(queryRegPattern.test(element[i])){
                    isThere= true
                    break;
                }
            }
            if(isThere){
                newProjectList.push(element)
            }
        })
        buildTable(newProjectList, projectEntry)

        let queryLength = newProjectList.length
        document.getElementById("status-bar").innerHTML = "Your query returned " + queryLength + " results using keyword '" + queryString + "' "
    })

})


    // named functions outside the main function



function buildTable(projectList, projectEntry){

    //remove tbody if one already exists and replaces it with an empty one

    if(document.getElementsByTagName("tbody")[0]){
        document.getElementsByTagName("tbody")[0].remove();
    }

    let tbody = document.createElement("tbody")
    document.getElementsByTagName("table")[0].appendChild(tbody)

    //build table based on array of projectList objects

    let table = document.getElementsByTagName("table")[0]

    for(let i = 0; i < projectList.length; i++){
        let keys = Object.keys(projectList[i])
        let tr = document.createElement("tr")
        tbody.appendChild(tr);
        for(let j = 0; j < 8; j++){
            let td = document.createElement("td")
            td.innerHTML = projectList[i][keys[j]]
            tr.appendChild(td)
        }

        //adds the save and trash icons at the end of the row -- also each icon is given an eventListener to give them functionality
        td = document.createElement("td")
        let img = document.createElement("img")
        td.appendChild(img)
        img.src = "../images/edit-icon.png"
        img.classList = "edit-icon"
        tr.appendChild(td)

        td = document.createElement("td")
        img = document.createElement("img")
        td.appendChild(img)
        img.src = "../images/trash.png"
        img.classList = "trash"
        tr.appendChild(td)

    }

    loopThroughTrashIcons(document.getElementsByClassName("trash"), projectList, projectEntry)
    loopThroughEditIcons(document.getElementsByClassName("edit-icon"), projectList, projectEntry)

}


function reset(formInputs){
        
    //loops through each input field, removes values and class from each, then disables the add button
        for(let input of formInputs){

            input.value = ''
            input.classList = ''
            try{
                input.nextElementSibling.remove()
                input.parentElement.nextSibling.remove()
                input.parentElement.nextElementSibling.remove()
                
            }
            catch(err){
                continue
            }
        }
        document.getElementById("addButton").disabled = true;
    }


//form validation

function validate(e, validation, formInputs){
   
    //create image for validation if it doesnt exists already
    let imgaeId= e.target.name+"ValidationImage",
        image= document.getElementById(imgaeId);
    if(!image){
        image= document.createElement("img");
        e.target.parentElement.appendChild(image);
        image.id=imgaeId;
        image.style.width="20%"
        image.style.height="50%"
    }
    
    //create text area for invalid if

    let divForText= document.createElement("Div");
    let textInvalid= document.createTextNode("Wrong format for " + e.target.name);
    let nonUnique = document.createTextNode("project id must be unique ")

    let divForTextId= e.target.name+"divForText"
    divForText.id=divForTextId;
    
    //return the regex for the target input
    var vads= validation.filter(vad=> vad.appliesTo.includes(e.target.name));

    for(let vad of vads){
       
        //if matches regex pattern and :valid is true then it checks!
        if(vad.regpattern.test(e.target.value) &&  e.target.validity.valid){
            e.target.classList.add("is-valid");
            e.target.classList.remove("is-invalid");
            image.src="../images/checkYes.png";

            //removes the red validation text below the form if it exists
            if(document.getElementById(divForTextId)){
                document.getElementById(divForTextId).remove()
            }
        } 
        else{

            // creates validtion text if element does not already exist
            if(!document.getElementById(divForTextId)){
                e.target.parentElement.parentElement.appendChild(divForText);
            }

            e.target.classList.add("is-invalid");
            e.target.classList.remove("is-valid");
            image.src="../images/checkNo.jpg";
            divForText.appendChild(textInvalid);
            divForText.style.backgroundColor="red";
            divForText.style.color="white";
            divForText.style.width="100%";
        }

    }



    if(e.target.id == "projid"){
        //validate if unique

        //validate if id is in table
        let allTableRows= document.getElementsByTagName("tr")

        for(let i=1; i<allTableRows.length; i++){
            if(e.target.value == allTableRows[i].firstChild.innerHTML){
                if(!document.getElementById(divForTextId)){
                    e.target.parentElement.parentElement.appendChild(divForText);
                }
                e.target.classList.add("is-invalid");
                e.target.classList.remove("is-valid");
                image.src="../images/checkNo.jpg";
                divForText.appendChild(nonUnique);
                divForText.style.backgroundColor="red";
                divForText.style.color="white";
                divForText.style.width="100%";
            }
        }

        //validate if id is in localStorage
        for (let i = 0; i < localStorage.length; i++) {
            //since we made the key a string with the projid, we just extract that projid
            let stringKey= "" + localStorage.key(i)
            if(e.target.value == stringKey.substring(10, )){
                if(!document.getElementById(divForTextId)){
                    e.target.parentElement.parentElement.appendChild(divForText);
                }
                e.target.classList.add("is-invalid");
                e.target.classList.remove("is-valid");
                image.src="../images/checkNo.jpg";
                divForText.appendChild(nonUnique);
                divForText.style.backgroundColor="red";
                divForText.style.color="white";
                divForText.style.width="100%";
            }
          }

    }
    
      // loop through the inputs to validate the classList is all 'is-valid'
      // something like this, code can be optimized BUT it works :)

    let allValid = true;

    for(let inputs of formInputs){
        if(inputs.className == "is-invalid" || inputs.className == ""){
            allValid = false;
            break;
        }
        allValid = true;
    }

    if(allValid){
        document.getElementById("addButton").disabled = false;
    }
    else{
        document.getElementById("addButton").disabled = true;
    }

}

function loopThroughTrashIcons(trashList, projectList, projectEntry){
  
    for(let i = 0; i < trashList.length; i++){
        trashList[i].addEventListener('click', function(e){
            if(confirm("Are you sure you want to delete this entry?")){
    
                //gets the id of the target trash bin
                let target = e.target.parentElement.parentElement.firstChild.innerHTML
    
                //for removing in local storage
                let keyName= " project: "+ target
                console.log(keyName)

                let updateProjectList = projectList.filter( projectEntry => {
                    return projectEntry.proj_id != target
                })
    
                projectList = updateProjectList
    
                buildTable(projectList, projectEntry)

                //remove trashed row from local storage
                localStorage.removeItem(keyName)
                
            }
            else{
            }
    
        })
    }
    
}

function loopThroughEditIcons(editList, projectList, projectEntry){

        for(let i = 0; i < editList.length; i++){
            editList[i].addEventListener('click', function(e){
                let tds= e.target.parentElement.parentElement.children
                let oldproject
                if(editList[i].id=="editingToSave"){
                    editList[i].id=''
                    for(let i=0; i<tds.length-2; i++){

                        let newValue= tds[i].firstChild.value
                        tds[i].removeChild(tds[i].firstChild)
                        tds[i].innerHTML=newValue
                    }
                    //no need to validate data
                    //now that it's saved, add it to array of projects
                    newproject= new projectEntry(tds[0].innerHTML, tds[1].innerHTML, tds[2].innerHTML, tds[3].innerHTML, tds[4].innerHTML, tds[5].innerHTML, tds[6].innerHTML, tds[7].innerHTML)
                    projectList.splice(projectList.indexOf(oldproject), 1, newproject)


                    editList[i].src="../images/edit-icon.png"

                }else{
                    editList[i].id="editingToSave"
                    for(let i=0; i<tds.length-2; i++){
                        let initialValue= tds[i].innerHTML
                        //keep the old project info
                        oldproject= projectList.filter( projectEntry => {
                            return projectEntry.proj_id == tds[0].innerHTML
                        })

                        let input= document.createElement("input")
                        input.type="text"
                        input.value=initialValue
                        tds[i].innerHTML=''
                        tds[i].appendChild(input)   
                    }
                    
                    editList[i].src="../images/save_icon.png"
                }
            })
        }


}

